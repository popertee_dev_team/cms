/*
* Map Requests
* Description: Includes the declaration of the request functions for the map page
*/

export function getLocationData(id, yearAndMonth, provider) {
  // Using promise to handle the async properties of JavaScript
  return new Promise((resolve, reject) => {
    // Using Axios to send the request
    axiosDataApi({
      url: '/location/get-location',
      method: 'post',
      headers: {
        Authorization: 'Bearer ' + getCookie('accessToken'),
      },
      data: {
        id: id,
        yearAndMonth: yearAndMonth,
        provider: provider
      },
    }).then((response) => {
      resolve(response.data);
    }).catch((error) => {
      resolve(undefined)
    });
  });
}

export function getLocationWithGeoJsonData(id, yearAndMonth, provider) {
  // Using promise to handle the async properties of JavaScript
  return new Promise((resolve, reject) => {
    // Using Axios to send the request
    axiosDataApi({
      url: '/location/get-location-with-geo-json',
      method: 'post',
      headers: {
        Authorization: 'Bearer ' + getCookie('accessToken'),
      },
      data: {
        id: id,
        yearAndMonth: yearAndMonth,
        provider: provider
      },
    }).then((response) => {
      resolve(response.data);
    }).catch((error) => {
      resolve(undefined)
    });
  });
}

export function getSpaceData(id) {
  console.log(id)
  // Using promise to handle the async properties of JavaScript
  return new Promise((resolve, reject) => {
    // Using Axios to send the request
    axiosDataApi({
      url: '/space/data',
      method: 'post',
      headers: {
        Authorization: 'Bearer ' + getCookie('accessToken'),
      },
      data: {
        id: id,
      },
    }).then((response) => {
      resolve(response.data);
    }).catch((error) => {
      reject(error);
    });
  });
}


export function getCookie(name) {
  const v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
  return v ? v[2] : null;
}
