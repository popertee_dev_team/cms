// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

// Components
import './components'

// Plugins
import './plugins'

// Sync router with store
import { sync } from 'vuex-router-sync'

// Application imports
import App from './App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'
import legend from 'chartist-plugin-legend';
import tooltips from 'chartist-plugin-tooltip';
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';

// Sync store with router
sync(store, router)
Vue.use(VuetifyGoogleAutocomplete, {
});
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')

router.beforeEach((to, from, next) => {
	// also checkout the same in global App.vue created() method to handle the first load of restrcited routes
	if(store.getters.getAccessToken == undefined && store.getters.getAccessToken == null){
		next({ path: '/login' })
	} else if(store.getters.isAdmin == undefined && store.getters.isAdmin == 'false'){
		next({ path: '/login' })
	} else if(store.getters.isAdmin == 'true'){
	    axiosPopertee.defaults.headers.common['Authorization'] = `Bearer ${store.getters.getAccessToken}`
    	next()

	} else {
		next({ path: '/login' })
	}
})
