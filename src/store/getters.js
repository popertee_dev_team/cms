// https://vuex.vuejs.org/en/getters.html

export default {
  	isLoggedIn: state => state.isLoggedIn,
	getEmail: state => state.email,
	isPro: state => state.pro,
	isBeta: state => state.beta,
	isAdmin: state => state.admin,
	getIpAddress: state => state.ipAddress,
	getUserId: state => state.userId,
	getFirstName: state => state.firstName,
	getLastName: state => state.lastName,
	getRememberMe: state => state.rememberMe,
	getUserAgent: state => state.userAgent,
	getExp: state => state.exp,
	getAccessToken: state => state.accessToken,
	getRefreshToken: state => state.refreshToken,
	getWhen: state => state.when,
	getWho: state => state.who,
	getDiscoverWho: state => state.discoverWho,
}
