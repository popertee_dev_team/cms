// https://vuex.vuejs.org/en/mutations.html
import Cookies from 'js-cookie'
export const LOGIN = "LOGIN"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGOUT = "LOGOUT"
export const UPDATEBETA = "UPDATEBETA"
export const UPDATEPRO = "UPDATEPRO"
export const UPDATEADMIN = "UPDATEADMIN"
export const UPDATEIPADDRESS = "UPDATEIPADDRESS"
export const UPDATEFIRSTNAME = "UPDATEFIRSTNAME"
export const UPDATELASTNAME = "UPDATELASTNAME"
export const UPDATEUSERAGENT = "UPDATEUSERAGENT"
export const UPDATEEXP = "UPDATEEXP"
export const UPDATEACCESSTOKEN = "UPDATEACCESSTOKEN"
export const UPDATEREFRESHTOKEN = "UPDATEREFRESHTOKEN"
export const UPDATEWHEN = "UPDATEWHEN"
export const UPDATEWHO = "UPDATEWHO"
export const UPDATEDISCOVERWHO = "UPDATEDISCOVERWHO"


export default {
  	[LOGIN] (state) {
  		state.pending = true
	},
	[LOGIN_SUCCESS] (state) {
		state.isLoggedIn = true
		state.pro = Cookies.get("pro")
		state.beta = Cookies.get("beta")
		state.admin = Cookies.get("admin")
		state.ipAddress = Cookies.get("ipAddress")
		state.userId = Cookies.get("userId")
		state.firstName = Cookies.get("firstName")
		state.lastName = Cookies.get("lastName")
		state.userAgent = Cookies.get("userAgent")
		state.exp = Cookies.get("exp")
		state.rememberMe = Cookies.get("rememberMe")
		state.accessToken = Cookies.get("accessToken")
      	state.refreshToken = Cookies.get("refreshToken")
      	state.email = Cookies.get("email")
      	state.pending = false

	},
	[LOGOUT](state) {
		state.isLoggedIn = false
		state.beta = 'false'
		state.pro = 'false'
		state.admin = 'false'
		// state.ipAddress = null
		state.userId = null
		state.firstName = null
		state.lastName = null
		state.userAgent = null
		state.exp = null
		state.rememberMe = 'false'
		state.accessToken = null
		state.refreshToken = null
        state.when = null
        state.who = null
		state.discoverWho = null
		// state.email =  null
	},
	[UPDATEBETA](state) {
		state.beta = Cookies.get("beta")
	},
	[UPDATEPRO](state) {
		state.pro = Cookies.get("pro")
	},
	[UPDATEADMIN](state) {
		state.admin = Cookies.get("admin")
	},
	[UPDATEIPADDRESS](state) {
		state.ipAddress = Cookies.get("ipAddress")
	},
	[UPDATEFIRSTNAME](state) {
		state.firstName = Cookies.get("firstName")
	},
	[UPDATELASTNAME](state) {
		state.lastName = Cookies.get("lastName")
	},
	[UPDATEUSERAGENT](state) {
		state.userAgent = Cookies.get("userAgent")
	},
    [UPDATEWHEN](state) {
  		state.when = Cookies.get("when")
	},
    [UPDATEWHO](state) {
  		state.who = Cookies.get("who")
	},
	[UPDATEDISCOVERWHO](state) {
		state.discoverWho = Cookies.get("discoverWho")
	},
	[UPDATEEXP](state) {
		state.exp = Cookies.get("exp")
	},
	[UPDATEACCESSTOKEN](state) {
		state.accessToken = Cookies.get("accessToken")
	},
	[UPDATEREFRESHTOKEN](state) {
		state.refreshToken = Cookies.get("refreshToken")
	},
}
