// https://vuex.vuejs.org/en/actions.html
import Cookies from 'js-cookie'
export const LOGIN = "LOGIN"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGOUT = "LOGOUT"
export const UPDATEBETA = "UPDATEBETA"
export const UPDATEPRO = "UPDATEPRO"
export const UPDATEADMIN = "UPDATEADMIN"
export const UPDATEIPADDRESS = "UPDATEIPADDRESS"
export const UPDATEFIRSTNAME = "UPDATEFIRSTNAME"
export const UPDATELASTNAME = "UPDATELASTNAME"
export const UPDATEUSERAGENT = "UPDATEUSERAGENT"
export const UPDATEEXP = "UPDATEEXP"
export const UPDATEACCESSTOKEN = "UPDATEACCESSTOKEN"
export const UPDATEREFRESHTOKEN = "UPDATEREFRESHTOKEN"
export const UPDATEWHEN = "UPDATEWHEN"
export const UPDATEWHO = "UPDATEWHO"
export const UPDATEDISCOVERWHO = "UPDATEDISCOVERWHO"
// import Cookies from 'js-cookie'

export default {
   login({ commit }, user) {
		commit(LOGIN)
		return new Promise(resolve => {
			Cookies.set("userId", user.id)
			Cookies.set("firstName", user.firstName)
			Cookies.set("userAgent", user.userAgent)
			Cookies.set("lastName", user.lastName)
			Cookies.set("exp", user.exp)
			Cookies.set("beta", user.beta)
			Cookies.set("pro", user.pro)

			var isAdmin = user.roles.includes("ADMIN")
			if(isAdmin) {
				Cookies.set("admin", "true")
			} else {
				Cookies.set("admin", "false")
			}



			Cookies.set("rememberMe", user.rememberMe)
            Cookies.set("email", user.email)
            Cookies.set("accessToken", user.accessToken)
            Cookies.set("refreshToken", user.refreshToken)
			commit(LOGIN_SUCCESS)
			resolve()
		})
   	},
	updateFirstName({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('firstName', payload)
			commit(UPDATEFIRSTNAME)
			resolve()
		})
	},
	updateLastName({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('lastName', payload)
			commit(UPDATELASTNAME)
			resolve()
		})
	},
	updateUserAgent({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('userAgent', payload)
			commit(UPDATEUSERAGENT)
			resolve()
		})
	},
	updateBeta({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('beta', payload.beta)
			commit(UPDATEBETA)
			resolve()
		})
	},
	updateAdmin({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('admin', payload.admin)
			commit(UPDATEADMIN)
			resolve()
		})
	},
	updatePro({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('pro', payload.pro)
			commit(UPDATEPRO)
			resolve()
		})
	},
	updateIpAddress({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set('ipAddress', payload)
			commit(UPDATEIPADDRESS)
			resolve()
		})
	},
	updateWhen({ commit }, when) {
			return new Promise(resolve => {
	            Cookies.set('when', JSON.stringify(when))
				commit(UPDATEWHEN)
				resolve()
			})
		},
	updateWho({ commit }, who) {
			return new Promise(resolve => {
			Cookies.set('who', JSON.stringify(who))
			commit(UPDATEWHO)
			resolve()
		})
	},
	updateDiscoverWho({ commit }, discoverWho) {
		return new Promise(resolve => {
			Cookies.set('discoverWho', JSON.stringify(discoverWho))
			commit(UPDATEDISCOVERWHO)
			resolve()
		})
	},
	updateFromRouterAccessToken({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set("accessToken", payload.accessToken)
			commit(UPDATEACCESSTOKEN)
			resolve()
		})
	},
	updateFromRouterRefreshToken({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set("refreshToken", payload.refreshToken)
			commit(UPDATEREFRESHTOKEN)
			resolve()
		})
	},
	updateFromRouterExp({ commit }, payload) {
		return new Promise(resolve => {
			Cookies.set("exp", payload.exp)
			commit(UPDATEEXP)
			resolve()
		})
	},
	// async nuxtServerInit ({commit}) {
	 // 	// console.log("OKODKWO")
	 //    // let {data} = await axios.get('employees.json')
	 //    Cookies.set("accessToken", "WWERR")
	 //    commit('UPDATEACCESSTOKEN')
	 //  },
	updateAccessToken({ commit }, accessToken) {
		return new Promise(resolve => {
			Cookies.set("accessToken", accessToken)
			commit(UPDATEACCESSTOKEN)
			resolve()
		})
	},
	logout({ commit }) {
		Cookies.remove("userId")
		Cookies.remove("firstName")
		Cookies.remove("lastName")
		Cookies.remove("userAgent")
		Cookies.remove("exp")
		Cookies.remove("beta")
		Cookies.remove("pro")
		Cookies.remove("admin")
		// Cookies.remove("ipAddress")
		Cookies.remove("rememberMe")
		Cookies.remove("email")
		Cookies.remove("accessToken")
		Cookies.remove("refreshToken")
	    Cookies.remove("when")
	    Cookies.remove("who")
	    Cookies.remove("discoverWho")
		commit(LOGOUT)
	}
}
