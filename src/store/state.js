// https://vuex.vuejs.org/en/state.html
import Cookies from 'js-cookie'

export default {
  	isLoggedIn: !!Cookies.get("accessToken"),
	pro: Cookies.get("pro"),
	admin: Cookies.get("admin"),
	beta: Cookies.get("beta"),
	ipAddress: Cookies.get("ipAddress"),
	userId: Cookies.get("userId"),
	firstName: Cookies.get("firstName"),
	lastName: Cookies.get("lastName"),
	rememberMe: Cookies.get("rememberMe"),
	userAgent: Cookies.get("userAgent"),
	exp: Cookies.get("exp"),
	accessToken: Cookies.get("accessToken"),
	refreshToken: Cookies.get("refreshToken"),
	when: Cookies.get("when"),
    who: Cookies.get("who"),
    discoverWho: Cookies.get("discoverWho"),
	email: Cookies.get("email"),
}
