import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$http = axios;
var axiosPopertee = '';
var axiosDataApi = '';
var axiosGeolocation = '';
// alert(JSON.stringify(process.env))
console.log(JSON.stringify(process.env));
// console.log(JSON.stringify(process.env.NODE_ENV))
var axiosPoperteeStaging = axios.create({baseURL: 'https://stagingapi.popertee.com'});
if (process.env.NODE_ENV == 'local') {
  axiosPopertee = axios.create({baseURL: 'http://localhost:8088'})
  axiosDataApi = axios.create({baseURL: 'http://localhost:8097'})
} else if (process.env.NODE_ENV === 'development') {
  axiosPopertee = axios.create({baseURL: 'https://devapi.popertee.com'})
  axiosDataApi = axios.create({baseURL: 'https://devapi.popertee.com/data'})
} else if (process.env.NODE_ENV === 'staging') {
  axiosPopertee = axios.create({baseURL: 'https://stagingapi.popertee.com'})
  axiosDataApi = axios.create({baseURL: 'https://stagingapi.popertee.com/data'})
} else {
  axiosPopertee = axios.create({baseURL: 'https://prodapi.popertee.com'})
  axiosDataApi = axios.create({baseURL: 'https://prodapi.popertee.com/data'})

}
global.axiosPopertee = axiosPopertee
global.axiosDataApi = axiosDataApi
global.axiosGeolocation = axios.create({baseURL: 'https://geolocation.popertee.com',})
global.axiosPoperteeStaging = axiosPoperteeStaging;
