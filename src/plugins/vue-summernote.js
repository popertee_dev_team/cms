


import VueSummernote from 'vue-summernote'
import Vue from 'vue'
require('bootstrap')

require('bootstrap/dist/css/bootstrap.min.css')
require('summernote/dist/summernote.css')

Vue.use(VueSummernote, {
  dialogsFade: true
})
