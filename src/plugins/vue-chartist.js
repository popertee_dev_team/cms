import Vue from 'vue';

if (process.client) {
    Vue.use(require('vue-chartist'));
    require('chartist-plugin-legend');
    require('chartist-plugin-tooltip');
}