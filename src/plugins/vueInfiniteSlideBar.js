 import Vue from 'vue'
import InfiniteSlideBar from 'vue-infinite-slide-bar'

Vue.component('infinite-slide-bar', InfiniteSlideBar)
