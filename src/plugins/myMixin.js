import Vue from 'vue'

Vue.mixin({
  created: function () {
  },
  data() {
    return {
    }
  },
  methods: {
    timeConverter(UNIX_timestamp) {
      if (UNIX_timestamp === undefined || UNIX_timestamp === null) {
        return "No information";
      }
      var a = new Date(UNIX_timestamp * 1000)
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      var year = a.getFullYear()
      var month = months[a.getMonth()]
      var date = a.getDate()
      var hour = a.getHours()
      var min = a.getMinutes()
      var sec = a.getSeconds()
      var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec
      return time
    },
    newImageToThumbnailMedium: function (card, scraper) {
      if (scraper === undefined || scraper === 'popertee') {
        if (card === undefined || card == null) {
          return 'https://s3.eu-west-2.amazonaws.com/popertee/images/old/2017/07/THE-LEP-8.jpg'
        } else if (card.startsWith('old')) {
          return 'https://s3.eu-west-2.amazonaws.com/popertee/images/' + card
        } else {
          var f = card.split('.')
          f = f[0] + '_md.' + f[1]
          return 'https://s3.eu-west-2.amazonaws.com/popertee/images/' + f
        }
      } else {
        return card;
      }
    },
    imageToThumbnailMedium: function (card) {
      return 'https://s3.eu-west-2.amazonaws.com/popertee/images/' + card
    },
  }
})