/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  // Main Single Routes
  {path: '/login', view: 'Login'},
  {path: '/dashboard', view: 'Dashboard'},
  {path: '/test', view: 'Test'},
  {path: '/logout', view: 'Logout'},
  // User CRUD
  {path: '/users', view: 'Users'},
  {path: '/users/create', view: 'UserCreate'},
  {path: '/users/details', view: 'UserDetails'},
  // Enquires CRUD
  {path: '/enquires', view: 'Enquires'},
  {path: '/enquires/details', view: 'EnquiryDetails'},
  // Careers CRUD
  {path: '/careers', view: 'Careers'},
  {path: '/careers/create', view: 'CareerCreate'},
  {path: '/careers/details', view: 'CareerDetails'},
  // Team CRUD
  {path: '/team', view: 'Team'},
  {path: '/team/create', view: 'TeamCreate'},
  {path: '/team/details', view: 'TeamDetails'},
  // Partners CRUD
  {path: '/partners', view: 'Partners'},
  {path: '/partners/details', view: 'PartnerDetails'},
  {path: '/partners/create', view: 'PartnerCreate'},
  // Spaces CRUD
  {path: '/spaces', view: 'Spaces'},
  {path: '/spaces/create', view: 'SpaceCreate'},
  {path: '/spaces/details', view: 'SpaceDetails'},
  {path: '/spaces/images', view: 'ImageReorder'},
  {path: '/scrapers', view: 'Scrapers'},
  {path: '/featuredSpaces', view: 'FeaturedSpaces'},
  {path: '/spaces/data', view: 'SpaceData'},

  // Reports CRUD
  {path: '/reports', view: 'Reports'},
  // NewsLetter CRUD
  {path: '/newsletter', view: 'Newsletter'},
  // Blog CRUD
  {path: '/blog', view: 'Blog'},
  {path: '/blog/create', view: 'BlogPostCreate'},
  {path: '/blog/details', view: 'BlogPostDetails'},
  // CaseStudies CRUD
  {path: '/caseStudies', view: 'CaseStudies'},
  {path: '/caseStudies/create', view: 'CaseStudyCreate'},
  {path: '/caseStudies/details', view: 'CaseStudyDetails'},

  // // Locations CRUD
  // {path: '/locations-xmode', view: 'LocationsXmode'},
  // {path: '/locations-predicio', view: 'LocationsPredicio'},
  // {path: '/locations/create', view: 'LocationCreate'},
  // {path: '/locations/geojson/create', view: 'LocationWithGeoJsonCreate'},
  // {path: '/locations/details', view: 'LocationDetails'}

]
